// Vonc - Mati�re D�grad� 3D - 1.0

#include "c4d.h"
#include "c4d_symbols.h"
#include "matdeg3d.h"

struct GradientData
{
	Float			turbulence, octaves, echelle, freq;
	Bool			absolu;

	Gradient*	degrade[3];
};

Float Mix(Float a, Float b, Float t) {

	return a + (b - a) * t;
}

class SDKGradientClass : public ShaderData
{
	private:
		static const char U = 0;
		static const char V = 1;
		static const char W = 2;
		GradientData ddeg;
		Vector		 echelle;
		Vector		 decalage;
		Bool		 repete;
		Int32		 mode;
		Int32		 melange;

	public:
		void InitDeg(BaseContainer *data, Int32 id);

	public:
		virtual Bool Init(GeListNode *node);
		virtual	Vector Output(BaseShader *sh, ChannelData *cd);
		virtual	INITRENDERRESULT InitRender(BaseShader *sh, const InitRenderStruct &irs);
		virtual	void FreeRender(BaseShader *sh);

		static NodeData* Alloc(void) { return NewObjClear(SDKGradientClass); }
};

void SDKGradientClass::InitDeg(BaseContainer *donnees, Int32 id)
{
    AutoAlloc<Gradient> degrade;
	if (!degrade) return;
    
	GradientKnot k1,k2;
	k1.col = Vector(1.0, 1.0, 1.0);
	k1.pos = 0.0;
    
	k2.col = Vector(0.0, 0.0, 0.0);
	k2.pos = 1.0;
    
	degrade->InsertKnot(k1);
	degrade->InsertKnot(k2);
    
	donnees->SetData(id, GeData(CUSTOMDATATYPE_GRADIENT, degrade));
}

Bool SDKGradientClass::Init(GeListNode *noeud)
{
	BaseContainer *donnees = ((BaseShader*)noeud)->GetDataInstance();

	this->InitDeg(donnees, VONCMATDEG_DEGU);
    this->InitDeg(donnees, VONCMATDEG_DEGV);
    this->InitDeg(donnees, VONCMATDEG_DEGW);

	donnees->SetVector(VONCMATDEG_ECH, Vector(1.0));
	donnees->SetVector(VONCMATDEG_DEC, Vector());
	donnees->SetInt32(VONCMATDEG_MODE, 0);
	donnees->SetInt32(VONCMATDEG_MEL, 0);
	donnees->SetBool(VONCMATDEG_REP, TRUE);
	
	donnees->SetFloat(VONCMATDEG_TURB, 0.0);
	donnees->SetFloat(VONCMATDEG_TURB_OCTAVES, 5.0);
	donnees->SetFloat(VONCMATDEG_TURB_ECH, 1.0);
	donnees->SetFloat(VONCMATDEG_TURB_FREQ, 1.0);
	donnees->SetBool(VONCMATDEG_TURB_ABSOLU, FALSE);

	return TRUE;
}

INITRENDERRESULT SDKGradientClass::InitRender(BaseShader *sh, const InitRenderStruct &irs)
{
	BaseContainer *donnees = sh->GetDataInstance();
	
	ddeg.turbulence = donnees->GetFloat(VONCMATDEG_TURB);
	ddeg.octaves    = donnees->GetFloat(VONCMATDEG_TURB_OCTAVES);
	ddeg.echelle      = donnees->GetFloat(VONCMATDEG_TURB_ECH);
	ddeg.freq       = donnees->GetFloat(VONCMATDEG_TURB_FREQ);
	ddeg.absolu   = donnees->GetBool(VONCMATDEG_TURB_ABSOLU);
	
	echelle = donnees->GetVector(VONCMATDEG_ECH);
	decalage = donnees->GetVector(VONCMATDEG_DEC);
	repete = donnees->GetBool(VONCMATDEG_REP);
	mode = donnees->GetInt32(VONCMATDEG_MODE);
	melange = donnees->GetInt32(VONCMATDEG_MEL);

	iferr_scope_handler { return INITRENDERRESULT::OUTOFMEMORY; };

	ddeg.degrade[U]	 = (Gradient*)donnees->GetCustomDataType(VONCMATDEG_DEGU, CUSTOMDATATYPE_GRADIENT); 
	ddeg.degrade[U]->InitRender(irs) iferr_return;

	if (!ddeg.degrade[U] /*|| !ddeg.degrade[U]->InitRender(irs)*/) return INITRENDERRESULT::OUTOFMEMORY;

	ddeg.degrade[V]	 = (Gradient*)donnees->GetCustomDataType(VONCMATDEG_DEGV, CUSTOMDATATYPE_GRADIENT);
	ddeg.degrade[V]->InitRender(irs) iferr_return;
	if (!ddeg.degrade[V] /*|| !ddeg.degrade[V]->InitRender(irs)*/) return INITRENDERRESULT::OUTOFMEMORY;

	ddeg.degrade[W]	 = (Gradient*)donnees->GetCustomDataType(VONCMATDEG_DEGW, CUSTOMDATATYPE_GRADIENT); 
	ddeg.degrade[W]->InitRender(irs) iferr_return;
	if (!ddeg.degrade[W] /*|| !ddeg.degrade[W]->InitRender(irs)*/) return INITRENDERRESULT::OUTOFMEMORY;

	return INITRENDERRESULT::OK;
}

void SDKGradientClass::FreeRender(BaseShader *sh)
{
	if (ddeg.degrade[U]) ddeg.degrade[U]->FreeRender();
	ddeg.degrade[U] = NULL;

	if (ddeg.degrade[V]) ddeg.degrade[V]->FreeRender();
	ddeg.degrade[V] = NULL;

	if (ddeg.degrade[W]) ddeg.degrade[W]->FreeRender();
	ddeg.degrade[W] = NULL;
}

Vector SDKGradientClass::Output(BaseShader *sh, ChannelData *sd)
{
	Vector secours = Vector(1.0, 0.0, 0.0);
	Vector p = sd->p;

	if (mode == 1 && sd->vd) p = sd->vd->p;

	
	if (echelle.x == 0.0) echelle.x = 1.0;
	if (echelle.y == 0.0) echelle.y = 1.0;
	if (echelle.z == 0.0) echelle.z = 1.0;

	p.x /= echelle.x;
	p.y /= echelle.y;
	p.z /= echelle.z;

	p += decalage;

	
	if (ddeg.turbulence>0.0)
	{
		Vector	res;
		Float		scl = 5.0*ddeg.echelle, tt = sd->t*ddeg.freq*0.3;

		res = Vector(Turbulence(p*scl,tt,ddeg.octaves,TRUE), Turbulence((p+Vector(0.34,13.0,2.43))*scl,tt,ddeg.octaves,TRUE), Turbulence((p+Vector(2.32,1.88,4.51))*scl,tt,ddeg.octaves,TRUE));

		if (ddeg.absolu)
		{
			p.x  = Mix(p.x,res.x,ddeg.turbulence);
			p.y  = Mix(p.y,res.y,ddeg.turbulence);
			p.z  = Mix(p.z,res.z,ddeg.turbulence);
		}
		else
		{
			p.x += (res.x-0.5)*ddeg.turbulence;
			p.y += (res.y-0.5)*ddeg.turbulence;
			p.z += (res.z-0.5)*ddeg.turbulence;
		}
	}

	if (repete) {
		p.x = FMod(p.x, 1);
		p.y = FMod(p.y, 1);
		p.z = FMod(p.z, 1);
		p.x = fabs(p.x);
		p.y = fabs(p.y);
		p.z = fabs(p.z);
	}

	Vector u = ddeg.degrade[U]->CalcGradientPixel(p.x);
	Vector v = ddeg.degrade[V]->CalcGradientPixel(p.y);
	Vector w = ddeg.degrade[W]->CalcGradientPixel(p.z);

	if (melange == 0) {
		p = u * v * w;
	}
	else if (melange == 1) {
		p = u + v + w;
	}
	else if (melange == 2) {
		p = (u + v + w) * Vector(0.333333);
	}
	
	return p;
}

Bool InscrireMatDeg(void)
{
	return RegisterShaderPlugin(1029051, GeLoadString(IDS_VONCMATDEG), 0, SDKGradientClass::Alloc, "matdeg3d"_s, 0);
}
