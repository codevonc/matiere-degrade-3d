#include "c4d.h"

Bool InscrireMatDeg(void);

Bool PluginStart(void)
{
	if (!InscrireMatDeg()) return false;
	return true;
}

void PluginEnd(void)
{
}

Bool PluginMessage(Int32 id, void *data)
{
	switch (id) {
	case C4DPL_INIT_SYS:
		if (!g_resource.Init()) return false;
		return true;
	}
	return false;
}